## Intro

This playbook builds an IP Fabric based on VX Cumulus. All the configuration should be stored in config/globals.yml

## Getting started
* VX Cumulus Virtualbox image: https://drive.google.com/drive/folders/1XxS0qn4oU3SXQUo955IZo_X0DwZ8CcwI?usp=sharing
* Ansible module: https://docs.ansible.com/ansible/2.7/modules/nclu_module.html#nclu-module

