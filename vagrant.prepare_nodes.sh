#!/bin/sh

HOSTS='k8s-master-l1-1 k8s-node-l1-1 k8s-node-l1-2 k8s-node-l2-1 k8s-node-l2-2'

for i in $HOSTS; do
    vagrant ssh $i -c 'sudo sh k8s-provisioning/01_ip.k8s-node.sh'
    vagrant ssh $i -c 'sudo sh k8s-provisioning/02_etc.hosts.k8s-nodes.sh'
    vagrant ssh $i -c 'sudo sh k8s-provisioning/03_docker.k8s-nodes.sh'
    vagrant ssh $i -c 'sudo sh k8s-provisioning/04_init.k8s-nodes.sh'
done


vagrant ssh k8s-master-l1-1 -c 'sudo sh k8s-provisioning/05_kubeadm-init.sh'

vagrant ssh k8s-master-l1-1 -c "mkdir -p \$HOME/.kube"
vagrant ssh k8s-master-l1-1 -c "sudo cp -i /etc/kubernetes/admin.conf \$HOME/.kube/config"
vagrant ssh k8s-master-l1-1 -c "sudo chown \$(id -u):\$(id -g) \$HOME/.kube/config"




