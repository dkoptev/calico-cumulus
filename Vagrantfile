# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  # IP Fabric
  config.vm.define("spine1") do |spine1|
    spine1.vm.box = "CumulusCommunity/cumulus-vx"
    spine1.vm.box_version = "3.7.3"
    spine1.vm.network("forwarded_port", guest: 22, host: 2022, id: 'ssh', auto_correct: false)

    # Fabric links (swp1 - swp4)
    spine1.vm.network("private_network", virtualbox__intnet: "SPINE1_LEAF1", cumulus__intname: 'swp1', nic_type: "virtio", auto_config: false)
    spine1.vm.network("private_network", virtualbox__intnet: "SPINE1_LEAF2", cumulus__intname: 'swp2', nic_type: "virtio", auto_config: false)
    spine1.vm.network("private_network", virtualbox__intnet: "SPINE1_LEAF3", cumulus__intname: 'swp3', nic_type: "virtio", auto_config: false)
    spine1.vm.network("private_network", virtualbox__intnet: "SPINE1_LEAF4", cumulus__intname: 'swp4', nic_type: "virtio", auto_config: false)
  end

  config.vm.define("spine2") do |spine2|
    spine2.vm.box = "CumulusCommunity/cumulus-vx"
    spine2.vm.box_version = "3.7.3"
    spine2.vm.network("forwarded_port", guest: 22, host: 2023, id: 'ssh', auto_correct: false)

    spine2.vm.network("private_network", virtualbox__intnet: "SPINE2_LEAF1", cumulus__intname: 'swp1', nic_type: "virtio", auto_config: false)
    spine2.vm.network("private_network", virtualbox__intnet: "SPINE2_LEAF2", cumulus__intname: 'swp2', nic_type: "virtio", auto_config: false)
    spine2.vm.network("private_network", virtualbox__intnet: "SPINE2_LEAF3", cumulus__intname: 'swp3', nic_type: "virtio", auto_config: false)
    spine2.vm.network("private_network", virtualbox__intnet: "SPINE2_LEAF4", cumulus__intname: 'swp4', nic_type: "virtio", auto_config: false)

  end

  config.vm.define("leaf1") do |leaf1|
    leaf1.vm.box = "CumulusCommunity/cumulus-vx"
    leaf1.vm.box_version = "3.7.3"    
    leaf1.vm.network("forwarded_port", guest: 22, host: 2024, id: 'ssh', auto_correct: false)

    leaf1.vm.network("private_network", virtualbox__intnet: "SPINE1_LEAF1", cumulus__intname: 'swp1', nic_type: "virtio", auto_config: false)
    leaf1.vm.network("private_network", virtualbox__intnet: "SPINE2_LEAF1", cumulus__intname: 'swp2', nic_type: "virtio", auto_config: false)

    # K8S on leaf1
    leaf1.vm.network("private_network", virtualbox__intnet: "K8S_LEAF1_MASTER1", cumulus__intname: 'swp3', nic_type: "virtio", auto_config: false)
    leaf1.vm.network("private_network", virtualbox__intnet: "K8S_LEAF1_NODE1", cumulus__intname: 'swp4', nic_type: "virtio", auto_config: false)
    leaf1.vm.network("private_network", virtualbox__intnet: "K8S_LEAF1_NODE2", cumulus__intname: 'swp5', nic_type: "virtio", auto_config: false)

  end

  config.vm.define("leaf2") do |leaf2|
    leaf2.vm.box = "CumulusCommunity/cumulus-vx"
    leaf2.vm.box_version = "3.7.3"
    leaf2.vm.network("forwarded_port", guest: 22, host: 2025, id: 'ssh', auto_correct: false)

    leaf2.vm.network("private_network", virtualbox__intnet: "SPINE1_LEAF2", cumulus__intname: 'swp1', nic_type: "virtio", auto_config: false)
    leaf2.vm.network("private_network", virtualbox__intnet: "SPINE2_LEAF2", cumulus__intname: 'swp2', nic_type: "virtio", auto_config: false)

    # K8S on leaf2
    leaf2.vm.network("private_network", virtualbox__intnet: "K8S_LEAF2_NODE1", cumulus__intname: 'swp3', nic_type: "virtio", auto_config: false)
    leaf2.vm.network("private_network", virtualbox__intnet: "K8S_LEAF2_NODE2", cumulus__intname: 'swp4', nic_type: "virtio", auto_config: false)

  end

  # Kubernetes 
  config.vm.define("k8s-master-l1-1") do |k8smaster1|
    k8smaster1.vm.box = "centos/7"
    k8smaster1.vm.hostname = "k8s-master-l1-1"
    k8smaster1.vm.network("forwarded_port", guest: 22, host: 3001, id: 'ssh', auto_correct: false)
    k8smaster1.vm.network("private_network", virtualbox__intnet: "K8S_LEAF1_MASTER1", nic_type: "virtio")
    k8smaster1.vm.provider "virtualbox" do |vb|
      vb.cpus = 4
      vb.memory = 4096
    end 

    k8smaster1.vm.provision "file", source: "k8s-provisioning/ip.k8s-master-l1-1.sh", destination: "$HOME/k8s-provisioning/01_ip.k8s-node.sh"
    k8smaster1.vm.provision "file", source: "k8s-provisioning/etc.hosts.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/02_etc.hosts.k8s-nodes.sh"
    k8smaster1.vm.provision "file", source: "k8s-provisioning/docker.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/03_docker.k8s-nodes.sh"
    k8smaster1.vm.provision "file", source: "k8s-provisioning/init.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/04_init.k8s-nodes.sh"
    k8smaster1.vm.provision "file", source: "k8s-provisioning/kubeadm-init.sh", destination: "$HOME/k8s-provisioning/05_kubeadm-init.sh"
    k8smaster1.vm.provision "file", source: "k8s-provisioning/labels.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/06_labels.k8s-nodes.sh"
    k8smaster1.vm.provision "file", source: "calico-with-peering/", destination: "$HOME/calico-with-peering"
    k8smaster1.vm.provision "file", source: "calico-vxlan-overlay/", destination: "$HOME/calico-vxlan-overlay"
    k8smaster1.vm.provision "file", source: "demo/", destination: "$HOME/demo"
    k8smaster1.vm.provision "file", source: "demo-multicast/", destination: "$HOME/demo-multicast"
  end

  config.vm.define("k8s-node-l1-1") do |k8snode1|
    k8snode1.vm.box = "centos/7"
    k8snode1.vm.hostname = "k8s-node-l1-1"
    k8snode1.vm.network("forwarded_port", guest: 22, host: 3002, id: 'ssh', auto_correct: false)
    k8snode1.vm.network("private_network", virtualbox__intnet: "K8S_LEAF1_NODE1", nic_type: "virtio")
    k8snode1.vm.provider "virtualbox" do |vb|
      vb.cpus = 1
      vb.memory = 1024
    end 
    k8snode1.vm.provision "file", source: "k8s-provisioning/ip.k8s-node-l1-1.sh", destination: "$HOME/k8s-provisioning/01_ip.k8s-node.sh"
    k8snode1.vm.provision "file", source: "k8s-provisioning/etc.hosts.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/02_etc.hosts.k8s-nodes.sh"
    k8snode1.vm.provision "file", source: "k8s-provisioning/docker.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/03_docker.k8s-nodes.sh"
    k8snode1.vm.provision "file", source: "k8s-provisioning/init.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/04_init.k8s-nodes.sh"
  end

  config.vm.define("k8s-node-l1-2") do |k8snode2|
    k8snode2.vm.box = "centos/7"
    k8snode2.vm.hostname = "k8s-node-l1-2"
    k8snode2.vm.network("forwarded_port", guest: 22, host: 3003, id: 'ssh', auto_correct: false)
    k8snode2.vm.network("private_network", virtualbox__intnet: "K8S_LEAF1_NODE2", nic_type: "virtio")
    k8snode2.vm.provider "virtualbox" do |vb|
      vb.cpus = 1
      vb.memory = 1024
    end 
    k8snode2.vm.provision "file", source: "k8s-provisioning/ip.k8s-node-l1-2.sh", destination: "$HOME/k8s-provisioning/01_ip.k8s-node.sh"
    k8snode2.vm.provision "file", source: "k8s-provisioning/etc.hosts.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/02_etc.hosts.k8s-nodes.sh"
    k8snode2.vm.provision "file", source: "k8s-provisioning/docker.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/03_docker.k8s-nodes.sh"
    k8snode2.vm.provision "file", source: "k8s-provisioning/init.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/04_init.k8s-nodes.sh"
  end

  config.vm.define("k8s-node-l2-1") do |k8snode3|
    k8snode3.vm.box = "centos/7"
    k8snode3.vm.hostname = "k8s-node-l2-1"
    k8snode3.vm.network("forwarded_port", guest: 22, host: 3004, id: 'ssh', auto_correct: false)
    k8snode3.vm.network("private_network", virtualbox__intnet: "K8S_LEAF2_NODE1", nic_type: "virtio")
    k8snode3.vm.provider "virtualbox" do |vb|
      vb.cpus = 1
      vb.memory = 1024
    end 
    k8snode3.vm.provision "file", source: "k8s-provisioning/ip.k8s-node-l2-1.sh", destination: "$HOME/k8s-provisioning/01_ip.k8s-node.sh"
    k8snode3.vm.provision "file", source: "k8s-provisioning/etc.hosts.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/02_etc.hosts.k8s-nodes.sh"
    k8snode3.vm.provision "file", source: "k8s-provisioning/docker.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/03_docker.k8s-nodes.sh"
    k8snode3.vm.provision "file", source: "k8s-provisioning/init.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/04_init.k8s-nodes.sh"
  end

  config.vm.define("k8s-node-l2-2") do |k8snode4|
    k8snode4.vm.box = "centos/7"
    k8snode4.vm.hostname = "k8s-node-l2-2"
    k8snode4.vm.network("forwarded_port", guest: 22, host: 3005, id: 'ssh', auto_correct: false)
    k8snode4.vm.network("private_network", virtualbox__intnet: "K8S_LEAF2_NODE2", nic_type: "virtio")
    k8snode4.vm.provider "virtualbox" do |vb|
      vb.cpus = 1
      vb.memory = 1024
    end 
    k8snode4.vm.provision "file", source: "k8s-provisioning/ip.k8s-node-l2-2.sh", destination: "$HOME/k8s-provisioning/01_ip.k8s-node.sh"
    k8snode4.vm.provision "file", source: "k8s-provisioning/etc.hosts.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/02_etc.hosts.k8s-nodes.sh"
    k8snode4.vm.provision "file", source: "k8s-provisioning/docker.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/03_docker.k8s-nodes.sh"
    k8snode4.vm.provision "file", source: "k8s-provisioning/init.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/04_init.k8s-nodes.sh"
  end

end
