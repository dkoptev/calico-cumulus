# Link on blog
- https://blog.container-solutions.com/prototyping-on-premises-network-infrastructure-for-workloads-running-on-kubernetes-clusters-part-2
- https://blog.container-solutions.com/prototyping-on-premises-network-infrastructure-for-workloads-running-on-kubernetes-clusters-part-3
- https://blog.container-solutions.com/prototyping-on-premises-network-infrastructure-for-kubernetes-clusters-part-4


# Kubernetes with Calico networking and Cumulus VX as IP Fabric

This repository contains demos for various deployment scenarios for Kubernetes with Calico and IP Fabric based on Cumulus VX.

The first step is to deploy IP Fabric, which is a common element for all scenarios. At minimum IP Fabric provides IP reachability between k8s nodes. In more advanced scenarios it peers with IP Fabric to exchange pod and service IP ranges.

Then in second step deploy one of the scenarios described below.

## High level diagram of the setup
![IP Fabric](images/ipfabric-generic.png "IP Fabric with K8s")

## Deploying IP Fabric with Cumulus VX

### Prerequisites
1. Install the latest Vagrant engine
2. Install Virtualbox 
3. 16GB of RAM and 4CPUs are recommended to run this demo
4. Install python 2.7+ with virtualenv

### Bringing IP Fabric up
1. Clone this repository and run following commands from inside of it
2. Bring all Cumulus VX VMs with ```vagrant up leaf1 spine1 leaf2 spine2``` command
3. IP Fabric: ```sh fabric.deploy.sh```

## Kubernetes and IP Fabric - deployment scenarios

#### Deploying cluster
1. Bring kubernetes VMs up with ```vagrant up k8s-master-l1-1 k8s-node-l1-1 k8s-node-l1-2 k8s-node-l2-1 k8s-node-l2-2``` command
2. Verify with ```vagrant status```
3. Prepare K8s nodes and take note of token and cert digest: ```sh vagrant.prepare_nodes.sh```
4. Join K8s workers to the master: ``` sh vagrant.join_nodes.sh <token> <cert digest>```

### Scenario 1 - Kubernetes nodes establish BGP peering with leaf switches, no overlay

In this scenario each kubernetes nodes establish single iBGP session with leaf switch to exchange pod and services prefixes. Workload traffic is a native IP traffic when it traverses IP-Fabric.

#### Highlevel diagram

![IP Fabric with BGP peering](images/ipfabric-with-peering.png)

#### Deploying networking
1. Deploy calico: ```sh vagrant.calico-with-peering.sh```

### Scenario 2 - Kubernetes nodes without BGP peering and VXLAN encapsulation in overlay

In this scenario each kubernetes nodes encapsulates workload traffic using VXLAN. No BGP peering is used.

#### Highlevel diagram

#### Deploying networking
1. Deploy calico: ```sh vagrant.calico-vxlan-overlay.sh```

## Testing and verification

### Deploying demo app
1. SSH to k8s-master-l1-1 node: ```vagrant ssh k8s-master-l1-1```
2. Execute: ```kubectl apply -f demo/namespace.yaml```
3. Execute: ```kubectl apply -f demo/deployment.yaml```
4. Execute: ```kubectl apply -f demo/service.yaml```

### Checking BGP peering in IP Fabirc
1. Execute: ```vagrant ssh leaf1 -c "sudo net show bgp sum"```
2. Execute: ```vagrant ssh leaf2 -c "sudo net show bgp sum"```
3. Execute: ```vagrant ssh spine1 -c "sudo net show bgp sum"```
4. Execute: ```vagrant ssh spine2 -c "sudo net show bgp sum"```

### Checking BGP prefixes in IP Fabirc
1. Execute: ```vagrant ssh leaf1 -c "sudo net show bgp"```
2. Execute: ```vagrant ssh leaf2 -c "sudo net show bgp"```
3. Execute: ```vagrant ssh spine1 -c "sudo net show bgp"```
4. Execute: ```vagrant ssh spine2 -c "sudo net show bgp"```
